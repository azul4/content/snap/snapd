# snapd

Service and tools for management of snap packages.

https://github.com/snapcore/snapd

<br><br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/snap/snapd.git
```

